using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Score : MonoBehaviour
{
    private int score;
    public TextMeshProUGUI uGUI;
    
    void start()
    {
        score = 0;
    }
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Coin")
        {
            score++;
            uGUI.text = "" + score;
            if(score == 5)
            {
                SceneManager.LoadScene("Two");
            }
            else
            {
                if(score == 4)
                {
                SceneManager.LoadScene("Win");
                }
            }
        }
    }
}
