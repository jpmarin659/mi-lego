using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class COIN : MonoBehaviour
{
    public AudioClip audioClip;
    private void OnTriggerEnter(Collider collision)
    {
        if(collision.gameObject.tag == "jugador")
        {
            AudioSource.PlayClipAtPoint(audioClip, gameObject.transform.position);
            if(audioClip == true)
            {
                Destroy(this.gameObject);
            }
        }
    }
}
