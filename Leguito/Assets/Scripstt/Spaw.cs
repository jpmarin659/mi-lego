using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spaw : MonoBehaviour
{
    [SerializeField] GameObject jugador;
    [SerializeField] List<GameObject> coin;
    [SerializeField] Vector3 point;
    [SerializeField] float rip;

    private void Update()
    {
        if(jugador.transform.position.y < -rip)
        {
            jugador.transform.position = point;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        point = jugador.transform.position;
        Destroy(other.gameObject);
    }
}
