using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimiento : MonoBehaviour
{
    public Animator anim;
    [Range(0,1)]
    public float movementSpeed;
    public float rotateSpeed;
    bool atacar = false;
    float acatarBueno = 2f;
    bool salto = false;
    bool saltoTrue = false;
    public float saltoBueno = 5f;
     Rigidbody rb;
    public AudioClip saltar;
    public AudioClip caminar;
    public void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    public void Update()
    {
        Vector3 movDirection = new Vector3(0,0,Input.GetAxis("Vertical"));
        anim.SetFloat("Speed", movDirection.z);
        transform.Translate((movDirection * movementSpeed) * Time.deltaTime);
        transform.Rotate(new Vector3(0, Input.GetAxis("Horizontal") * rotateSpeed, 0));
        Vector3 vector3 = transform.TransformDirection(Vector3.down);
        if (Input.GetButtonDown("Vertical"))
        {
            AudioSource.PlayClipAtPoint(caminar, gameObject.transform.position);
        }
        if (Physics.Raycast(transform.position, vector3,1))
        {
            salto = true;
        }
        else
        {
            salto = false;
        }
        saltoTrue = Input.GetButtonDown("Jump");
        if(saltoTrue && salto)
        {
            rb.AddForce(new Vector3(0, saltoBueno, 0), ForceMode.Impulse);
            anim.SetBool("Salto", true);
            AudioSource.PlayClipAtPoint(saltar, gameObject.transform.position);
        }
        if(!saltoTrue && salto)
        {
            anim.SetBool("Salto", false);
        }
        atacar = Input.GetButton("Fire1");
        if(atacar)
        {
            rb.AddForce(new Vector3(0, acatarBueno, 0), ForceMode.Force);
            anim.SetBool("atacar", true);
        }
        if(!atacar)
        {
            anim.SetBool("atacar", false);
        }
    }
}

